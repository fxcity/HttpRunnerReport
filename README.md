#httprunnerreport
一个由`HTMLTestRunner`项目为灵感，并基于`HTMLTestRunner`进行二次开发的一个项目。主要在API调用、报告样式、扩展性等方面进行了增强。

点击查看`HTMLTestRunner`的[官网](http://tungwaiyip.info/software/HTMLTestRunner.html)。`HTMLTestRunner`是基于Python单元测试官方实现的`TextTestResult`为参考，实现了对应的`HTMLTestResult`版本。

本项目是二次开发项目，原项目为PyTestReport（https://github.com/five3/PyTestReport）。在此项目基础上进行调整和适配。

现支持流行的自动化框架httprunner的应用，可查看theme下的效果图。

httprunner下的使用：
hrun xx.yml/xxx.py --pytest_report xxx.html
每次运行的报告名不能重复，html报告名可加时间戳区分。

# 安装与使用
## 安装
### 通过pip安装
```bash
pip install httprunner_report 
```

### 通过安装包
可通过发布的安装包进行安装，具体安装包可在dist目录查找。
```bash
pip install HttpRunnerReport-0.1.5-py3-none-any.whl
```

### 通过源码（最新版本）
```bash
pip install git+https://gitee.com/fxcity/HttpRunnerReport.git
```
或者
```bash
git clone https://gitee.com/fxcity/HttpRunnerReport.git
cd HttpRunnerReport
python setup.py build
python setup.py install
```

# 使用
HttpRunnerReport可用通过多种方式运行，分别如下：
- 单元测试(unittest, pytest) 
- lib库引入
- 命令行
- REST API

## 样例说明
### 单元测试样例(unittest)
```python
import unittest
import httprunner_report

class MyTest(unittest.TestCase):
    def testTrue(self):
        self.assertTrue(True)
        
if __name__ == '__main__':
    httprunner_report.main(verbosity=2)
```
以这种方式执行之后，默认会在当前文件夹下生成一个`httprunnerreport.html`日志文件，且这个文件名和样式模板都不可以重新指定的。

> 注意：这种方式执行时，如果使用Pycharm等IDE，确保不是以IDE的内建单元测试框架来执行的；或者直接通过命令行来执行。

```python
import unittest
from httprunner_report import TestRunner

class MyTest(unittest.TestCase):
    def testTrue(self):
        self.assertTrue(True)

if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(MyTest))
    
    with open(r'/path/to/report.html', 'wb') as fp:
        runner = TestRunner(fp, title='测试标题', description='测试描述', verbosity=2)
        runner.run(suite)
```
这种方式适合批量加载和执行测试用例，从测试文件的外部来导入测试用例并执行。这里可以指定具体的结果文件路径和测试标识等信息。

> 这里使用的是默认模板主题，如果想要使用其它模板主题，可以通过制定模板的主题文件来实现。比如：使用遗留模板的方式如下所示。
```python
from httprunner_report import TestRunner
...
runner = TestRunner(fp, title='测试标题', description='测试描述', verbosity=2, theme='legency')
```

### 单元测试样例（pytest）
对于pytest框架，收集其测试结果信息是通过pytest插件形式实现的。使用之前只要确保正常安装了httprunnerreport即可。具体使用方式如下：
```python
import pytest

def testTrue():
    assert True

def testFalse():
    assert False

def testError():
    1 / 0

@pytest.mark.skip(reason="misunderstood the API")
def testSkip():
    assert 1 == 1

@pytest.mark.xfail(reason="Xpass")
def testXPass():
    assert True

@pytest.mark.xfail(reason="Xfail")
def testXFail():
    assert False


if __name__ == '__main__':
    pytest.main(["-s", "pytest_Demo.py", "--pytest_report", "Pytest_Report.html"])
```
需要注意的是，pytest框架想要使用本测试报告框架，在调用时需要带上`--pytest_report`参数，并指定一个报告的文件路径即可。当然你也可以同时指定一个非默认主题。比如：
```python
import pytest

if __name__ == '__main__':
    pytest.main(["-s", "pytest_Demo.py", "--pytest_report", "Pytest_Report.html", 
    "--pytest_title", "report title", "--pytest_desc", "report desc",
    "--pytest_theme", "new_theme"])
```
另外，你也可以通过命令行的方式来启动pytest执行单元测试。比如：
```bash
pytest -s pytest_Demo.py --pytest_report Pytest_Report.html --pytest_theme new_theme
```