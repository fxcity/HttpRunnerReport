#!/usr/bin/env python
# coding=utf-8
from setuptools import setup, find_packages
from httprunner_report import HTMLTestRunner

# python setup.py sdist
# python setup.py bdist
# python setup.py bdist_egg
# python setup.py bdist_wheel
# twine upload dist/*0.1.4*

setup(
    name="HttpRunnerReport",
    version=HTMLTestRunner.__version__,
    keywords=("test report", "python unit testing"),
    description="The HTML Report for Python unit testing Base on HTMLTestRunner",
    long_description="The HTML Report for Python unit testing Base on HTMLTestRunner",
    license="GPL V3",

    url="https://gitee.com/fxcity/HttpRunnerReport.git",
    author="ylfeng",
    author_email="1390748563@qq.com",

    package_dir={'httprunner_report': 'httprunner_report'},         # 指定哪些包的文件被映射到哪个源码包
    packages=['httprunner_report'],       # 需要打包的目录。如果多个的话，可以使用find_packages()自动发现
    include_package_data=True,
    package_data={'httprunner_report': ['static/*', 'templates/*']},
    py_modules=[],          # 需要打包的python文件列表
    data_files=[            # 打包时需要打包的数据文件
        'httprunner_report/static/js/capture.js',
        'httprunner_report/templates/default.html',
        'httprunner_report/static/css/default.css',
        'httprunner_report/static/js/default.js',
        'httprunner_report/templates/legency.html',
        'httprunner_report/static/css/legency.css',
        'httprunner_report/static/js/legency.js'
    ],
    platforms="any",
    install_requires=[      # 需要安装的依赖包
        'Jinja2==2.11.2',
        'ansi2html==1.6.0',
        'Flask==1.1.2',
        'MarkupSafe==0.23'
    ],
    scripts=[],             # 安装时复制到PATH路径的脚本文件
    entry_points={
        'console_scripts': [    # 配置生成命令行工具及入口
            'HttpRunnerReport.shell = httprunner_report:shell',
            'HttpRunnerReport.web = httprunner_report:web'
        ], "pytest11": ["httprunner_report = httprunner_report.HttpRunnerReport"]
    },
    classifiers=[           # 程序的所属分类列表
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Framework :: Pytest"
    ],
    zip_safe=False
)
